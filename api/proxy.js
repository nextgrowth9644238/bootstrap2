// functions/proxy.js
const express = require("express");
const axios = require("axios");
const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());

app.post("/sendFormData", async (req, res) => {
  try {
    const response = await axios.post(
      "https://forms.maakeetoo.com/formapi/758",
      req.body
    );
    res.json(response.data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "An error occurred" });
  }
});

module.exports = app;
