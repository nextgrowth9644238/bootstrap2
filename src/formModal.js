let modal = document.getElementsByClassName("modal");
let modalOpenbuttonOne = document.getElementsByClassName("btn-outline-primary");
let modalOpenbuttonTwo = document.getElementsByClassName("get-started");
let modalOpenbuttonThree = document.getElementsByClassName("contact-us");
let closeModalBtn = document.getElementsByClassName("btn-close");
let formSubmitBtn = document.getElementById("myForm");
let thankYouMessage = document.querySelector(".thank-you-message");

// modalOpenbuttonOne.addEventListener("click", function () {
//   modal.style.display = "block";
//   console.log("Ho");
// });
modalOpenbuttonOne[0].addEventListener("click", function () {
  modal[0].style.display = "block";
});
modalOpenbuttonTwo[0].addEventListener("click", function () {
  modal[0].style.display = "block";
});
modalOpenbuttonThree[0].addEventListener("click", function () {
  modal[0].style.display = "block";
});

closeModalBtn[0].addEventListener("click", function () {
  modal[0].style.display = "none";
});
window.addEventListener("click", function (event) {
  if (event.target == "modal") {
    modal[0].style.display = "none";
  }
});

const userSlider = document.getElementById("userSlider");
const freePlan = document.getElementById("freePlan");
const proPlan = document.getElementById("proPlan");
const enterprisePlan = document.getElementById("enterprisePlan");
let cardheader = document.getElementsByClassName("card-header");

userSlider.addEventListener("input", () => {
  const users = parseInt(userSlider.value);

  if (users >= 0 && users < 10) {
    freePlan.style.border = "1px solid blue";
    freePlan.querySelector(".card-header").style.backgroundColor = "#027BFF";
    enterprisePlan.querySelector(".card-header").style.backgroundColor = "";
    proPlan.querySelector(".card-header").style.backgroundColor = "";
    proPlan.style.border = "";
    enterprisePlan.style.border = "";
  } else if (users >= 10 && users < 20) {
    freePlan.style.border = "";
    freePlan.querySelector(".card-header").style.backgroundColor = "";
    proPlan.style.border = "1px solid blue";
    proPlan.querySelector(".card-header").style.backgroundColor = "#027BFF";
    freePlan.querySelector(".card-header").style.backgroundColor = "";
    enterprisePlan.querySelector(".card-header").style.backgroundColor = "";
    enterprisePlan.style.border = "";
  } else {
    freePlan.style.border = "";
    proPlan.style.border = "";
    enterprisePlan.style.border = "1px solid blue";
    proPlan.querySelector(".card-header").style.backgroundColor = "";
    freePlan.querySelector(".card-header").style.backgroundColor = "";
    enterprisePlan.querySelector(".card-header").style.backgroundColor =
      "#027BFF";
  }
});

// formSubmitBtn[0].addEventListener("click", async function () {
//   const response = await fetch("https://forms.maakeetoo.com/formapi/758", {
//     method: "POST",
//     body: JSON.stringify({
//       firstname: "Hello",
//       email: "abcdef@ghi.jkl",
//       message: "Hello India",
//     }),
//     headers: {
//       "Content-type": "application/json; charset=UTF-8",
//       "Access-Control-Allow-Origin": "*",
//       AccessCode: "MY58ZUUQYU0G2SFKKIYDN0EVR",
//     },
//   });
// });

formSubmitBtn.addEventListener("submit", async function (e) {
  try {
    const formData = new FormData(this);
    const formDataObject = {};
    formData.forEach((value, key) => {
      formDataObject[key] = value;
    });

    console.log(formDataObject);
    e.preventDefault();
    const response = await fetch("/api/proxy/sendFormData", {
      method: "POST",
      body: JSON.stringify(formDataObject),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        // "Access-Control-Allow-Origin": "*",
        AccessCode: "MY58ZUUQYU0G2SFKKIYDN0EVR",
      },
    });
    if (response.ok) {
      thankYouMessage.style.display = "block";
      setTimeout(function () {
        modal.style.display = "none";
        thankYouMessage.style.display = "none";
      }, 3000);
    } else {
      console.log("An error occurred while sending the form data.");
    }
    // Display success message.
    alert("Form data sent successfully!");
  } catch (error) {
    // Display error message.
    console.log("An error occurred while sending the form data.");
  }
});
