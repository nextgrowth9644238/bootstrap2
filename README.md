# bootStrap2





## How I implemented

- Designed a Modal, which on clicking any of the 3 CTA's will trigger a eventlistener for "click" event & opens a Modal
 

 - We can fill form which will submit the enterd data into forms.maakeetoo.com API endpoint using fetch API with POST request

 - Created Slider which is a input element which on sliding will highlight the respective Pricing Plans

 - Checked the core Web Vitals in Lighthouse & optimised core Web vitals as well as SEO

 - Deplyment Link - https://bootstrap2-ai4tbhk4k-e44rfrtt5.vercel.app/

 - Since browser  security policy that restricts web pages from making requests to a different domain hence CORS error so I have tried implementing Server-Side Proxy in vercel to bypass CORS error 
  
  - So I have set up a server-side proxy on Vercel as a serverless function. This serverless function acts as an intermediary between my Vercel Deployment URL  (front-end) and an external API.